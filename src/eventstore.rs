use crate::domain::design::{EventRecord, EventStore};
use crate::domain::events::ProductCatalogEvent;

#[derive(Debug, Clone)]
pub struct CatalogEventStore {
    sequence_id: i64,
    agg_id: String,
    events: Vec<EventRecord<ProductCatalogEvent>>,
}

impl CatalogEventStore {
    pub fn new(agg_id: &str) -> Self {
        CatalogEventStore {
            sequence_id: 0i64,
            agg_id: String::from(agg_id),
            events: vec![],
        }
    }
}

impl EventStore for CatalogEventStore {
    type E = ProductCatalogEvent;

    fn add(&mut self, e: ProductCatalogEvent) {
        self.events.push(EventRecord::<ProductCatalogEvent>::new(
            self.sequence_id,
            self.agg_id.as_str(),
            e,
        ));
        self.sequence_id += 1;
    }

    fn read_all(&self) -> Vec<ProductCatalogEvent> {
        let mut res: Vec<ProductCatalogEvent> = vec![];
        for e in &self.events {
            res.push(e.event.clone());
        }
        res
    }
}
