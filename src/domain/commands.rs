use rust_decimal::Decimal;

use crate::domain::design::DomainCommand;

#[derive(Debug)]
pub enum ProductCatalogCmd {
    CreateProduct {
        product_id: String,
        name: String,
        brand: String,
        price: Decimal,
        sizes: Vec<String>,
        images: Vec<String>,
    },
    RemoveProduct {
        prod_id: String,
    },
}

impl DomainCommand for ProductCatalogCmd {}

impl ProductCatalogCmd {
    pub fn create(
        prod_id: &str,
        name: &str,
        brand: &str,
        price: Decimal,
        sizes: Vec<String>,
        images: Vec<String>,
    ) -> Self {
        Self::CreateProduct {
            product_id: String::from(prod_id),
            name: String::from(name),
            brand: String::from(brand),
            price,
            sizes,
            images,
        }
    }

    pub fn remove(prod_id: &str) -> ProductCatalogCmd {
        Self::RemoveProduct {
            prod_id: String::from(prod_id),
        }
    }
}
