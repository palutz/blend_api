use rust_decimal::Decimal;
use std::collections::hash_map::DefaultHasher;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

use serde::{Deserialize, Serialize};

use crate::domain::commands::ProductCatalogCmd;
use crate::domain::design::Aggregate;
use crate::domain::events::ProductCatalogEvent;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Catalog {
    catalog_id: String,
    name: String,
    inventory: HashMap<String, Product>,
}

impl Catalog {
    pub fn new(c_id: &str, name: &str) -> Self {
        Catalog {
            catalog_id: String::from(c_id),
            name: String::from(name),
            inventory: HashMap::new(),
        }
    }
    pub fn inventory(&self) -> Vec<Product> {
        let mut res: Vec<Product> = vec![];
        for p in &self.inventory {
            res.push(p.1.clone());
        }
        res
    }
}

impl Hash for Catalog {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.name.hash(state);
    }
}

impl Aggregate<ProductCatalogCmd, ProductCatalogEvent> for Catalog {
    fn aggregate_id(&self) -> String {
        if self.catalog_id.is_empty() {
            let mut hasher = DefaultHasher::new();
            self.hash(&mut hasher);
            hasher.finish().to_string()
        } else {
            self.catalog_id.clone()
        }
    }

    // Logic implementation for the Command
    // Receive a command, apply the logic in the current state and yield the events
    fn decide(&self, command: ProductCatalogCmd) -> Vec<ProductCatalogEvent> {
        match command {
            ProductCatalogCmd::CreateProduct {
                product_id,
                name,
                brand,
                price,
                sizes,
                images,
            } => {
                // check there is no other product with the same id
                if !self.inventory.contains_key(&product_id) {
                    let p = Product::new(
                        product_id.as_str(),
                        name.as_str(),
                        brand.as_str(),
                        price,
                        sizes,
                        images,
                    );
                    let pdata = ProductEventData::new(
                        p.product_id.as_str(),
                        p.name.as_str(),
                        p.brand.as_str(),
                        p.price,
                        p.sizes,
                        p.images,
                    );
                    let e = ProductCatalogEvent::ProductCreated { p_data: pdata };
                    vec![e] // new event created
                } else {
                    // don't override an existing product
                    vec![] // we should return an event (error or warning)
                }
            }
            ProductCatalogCmd::RemoveProduct { prod_id } => {
                if self.inventory.contains_key(&prod_id) {
                    let e = ProductCatalogEvent::ProductRemoved { prod_id };
                    vec![e]
                } else {
                    vec![]
                }
            }
        }
    }

    // Apply (process) the event(s) to change the state of the aggregate (produce a new one)
    fn apply(&self, p_ev: &ProductCatalogEvent) -> Self {
        let mut new_c = self.clone();
        match p_ev {
            ProductCatalogEvent::ProductCreated { p_data } => {
                let p: Product = ProductEventData::into(p_data.clone());
                new_c.inventory.insert(p.product_id().clone(), p);
            }
            ProductCatalogEvent::ProductRemoved { prod_id } => {
                new_c.inventory.remove(prod_id);
            }
        }
        new_c
    }
}

/// Product Struct
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Product {
    product_id: String,
    name: String,
    brand: String,
    price: Decimal,
    sizes: Vec<String>,
    images: Vec<String>,
}

impl Product {
    pub fn new(
        prod_id: &str,
        name: &str,
        brand: &str,
        price: Decimal,
        sizes: Vec<String>,
        images: Vec<String>,
    ) -> Self {
        Product {
            product_id: String::from(prod_id),
            name: String::from(name),
            brand: String::from(brand),
            price,
            sizes,
            images,
        }
    }

    pub fn product_id(&self) -> &String {
        &self.product_id
    }
}

impl PartialEq for Product {
    fn eq(&self, other: &Self) -> bool {
        self.product_id == *other.product_id()
    }

    // fn ne(&self, other: &Self) -> bool {
    //     self.product_id != *other.product_id()
    // }
}
impl From<ProductEventData> for Product {
    fn from(val: ProductEventData) -> Self {
        Product {
            product_id: val.product_id,
            name: val.name,
            brand: val.brand,
            price: val.price,
            sizes: val.sizes,
            images: val.images,
        }
    }
}

// TODO - Sizes (with price?) and images as value objects?

/// ProductEventData
#[derive(Debug, Clone)]
pub struct ProductEventData {
    product_id: String,
    name: String,
    brand: String,
    price: Decimal,
    sizes: Vec<String>,
    images: Vec<String>,
}

impl ProductEventData {
    pub fn new(
        prod_id: &str,
        name: &str,
        brand: &str,
        price: Decimal,
        sizes: Vec<String>,
        images: Vec<String>,
    ) -> Self {
        ProductEventData {
            product_id: String::from(prod_id),
            name: String::from(name),
            brand: String::from(brand),
            price,
            sizes,
            images,
        }
    }

    pub fn product_id(&self) -> &String {
        &self.product_id
    }
}

#[cfg(test)]
mod test_aggregate {
    use crate::domain::commands::ProductCatalogCmd::{CreateProduct, RemoveProduct};
    use crate::domain::design::Aggregate;
    use rust_decimal::Decimal;
    use rust_decimal_macros::dec;

    use super::Catalog;

    #[test]
    fn test_aggregate() {
        let mut mock_agg = Catalog::new("000", "mock");

        let p1: (&str, &str, &str, Decimal, Vec<String>, Vec<String>) = (
            "001",
            "Prod01",
            "Brand1",
            dec!(100.50),
            vec!["S".to_string(), "M".to_string()],
            vec![],
        );
        let p2: (&str, &str, &str, Decimal, Vec<String>, Vec<String>) =
            ("003", "Prod03", "Brand2", dec!(30.00), vec![], vec![]);

        let ev1 = mock_agg.decide(CreateProduct {
            product_id: p1.0.to_string(),
            name: p1.1.to_string(),
            brand: p1.2.to_string(),
            price: p1.3,
            sizes: p1.4.clone(),
            images: p1.5.clone(),
        });
        // adding one product, we expect one event back
        assert_eq!(1, ev1.len());
        // apply the event to the aggregate
        mock_agg = mock_agg.apply(ev1.first().unwrap());
        // adding the same product it ends up with no new event created
        let ev2 = mock_agg.decide(CreateProduct {
            product_id: p1.0.to_string(),
            name: p1.1.to_string(),
            brand: p1.2.to_string(),
            price: p1.3,
            sizes: p1.4,
            images: p1.5,
        });
        assert_eq!(0, ev2.len());

        // trying to remove a non exisiting product ends up in 0 events fired
        let ev3 = mock_agg.decide(RemoveProduct {
            prod_id: p2.0.to_string(),
        });
        assert_eq!(0, ev3.len());

        // deleting a product in teh aggregate produces a new event
        let ev4 = mock_agg.decide(RemoveProduct {
            prod_id: p1.0.to_string(),
        });
        assert_eq!(1, ev4.len());
        // nothing changed in the aggregate yet
        assert_eq!(1, mock_agg.inventory().len());
        // but after applying the right event, the aggregate is correctly updated
        mock_agg = mock_agg.apply(ev4.first().unwrap());
        assert_eq!(0, mock_agg.inventory().len()); // removed the product, no more item in the inventory
    }
}
