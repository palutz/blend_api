pub trait DomainCommand {}
pub trait DomainEvent {}
pub trait DomainView {}

pub trait Aggregate<C, E>
where
    C: DomainCommand,
    E: DomainEvent,
{
    fn aggregate_id(&self) -> String;
    fn decide(&self, command: C) -> Vec<E>;
    fn apply(&self, events: &E) -> Self;
}

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub struct EventRecord<E>
where
    E: DomainEvent,
{
    sequence_id: i64,
    pub aggregate_id: String,
    pub event: E,
}

impl<E> EventRecord<E>
where
    E: DomainEvent,
{
    pub fn new(seqid: i64, aggid: &str, e: E) -> Self {
        EventRecord {
            sequence_id: seqid,
            aggregate_id: String::from(aggid),
            event: e,
        }
    }
}

pub trait EventStore {
    type E: DomainEvent;
    fn add(&mut self, e: Self::E);
    fn read_all(&self) -> Vec<Self::E>;
}
