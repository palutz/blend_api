use rust_decimal::Decimal;

use crate::domain::aggregate::ProductEventData;
use crate::domain::design::DomainEvent;

#[derive(Debug, Clone)]
pub enum ProductCatalogEvent {
    ProductCreated { p_data: ProductEventData },
    ProductRemoved { prod_id: String },
}

impl DomainEvent for ProductCatalogEvent {}

impl ProductCatalogEvent {
    pub fn created(
        prod_id: &str,
        name: &str,
        brand: &str,
        price: Decimal,
        sizes: Vec<String>,
        images: Vec<String>,
    ) -> Self {
        Self::ProductCreated {
            p_data: ProductEventData::new(prod_id, name, brand, price, sizes, images),
        }
    }

    pub fn removed(prod_id: &str) -> Self {
        Self::ProductRemoved {
            prod_id: String::from(prod_id),
        }
    }

    pub fn product_id(&self) -> &String {
        match self {
            ProductCatalogEvent::ProductCreated { p_data } => p_data.product_id(),
            ProductCatalogEvent::ProductRemoved { prod_id, .. } => prod_id,
        }
    }
}
