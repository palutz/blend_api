use crate::domain::aggregate::{Catalog, Product};
use crate::domain::design::DomainView;

pub enum CatalogQuery {
    ListActiveProducts { agg: Catalog },
    ListDeletedProducts,
}

impl DomainView for CatalogQuery {}

impl CatalogQuery {
    pub fn execute(&self) -> Vec<Product> {
        match self {
            Self::ListActiveProducts { agg } => agg.inventory(),
            Self::ListDeletedProducts => vec![], //  TODO, query the event store
        }
    }
}
