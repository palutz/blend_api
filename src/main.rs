use blend_api::cqrs::Catalog_CQRS;
use blend_api::domain::aggregate::Catalog;
use blend_api::domain::commands::ProductCatalogCmd::{CreateProduct, RemoveProduct};
use blend_api::eventstore::CatalogEventStore;
use blend_api::queries::CatalogQuery::ListActiveProducts;
use rust_decimal::Decimal;
use rust_decimal_macros::dec;
use tokio::io::{AsyncBufReadExt, AsyncWriteExt, BufReader};
use tokio::net::{TcpListener, TcpStream};

#[tokio::main]
async fn main() {
    // creating the aggregate for the catalog and all the machinery
    let aggid = "agg001";
    let eventstore = CatalogEventStore::new(aggid);
    let catalog = Catalog::new(aggid, "Catalog01");

    // the connector between the DDD World and the rest
    let mut catalog_ddd = Catalog_CQRS::new(catalog, eventstore);

    // simple TCP server
    // use 0.0.0.0 for the tcplistener in docker 
    let listener = TcpListener::bind("0.0.0.0:3113").await.unwrap();
    println!("Blend api server running on port 3113");
    loop {
        let (stream, _) = listener.accept().await.unwrap();
        handle_connection(stream, &mut catalog_ddd).await;
    }
}

// mock data to insert product faster
fn mock_prod_data(id: &str) -> (&str, &str, &str, Decimal, Vec<String>, Vec<String>) {
    match id {
        "1" => (
            "001",
            "Prod01",
            "Brand1",
            dec!(100.50),
            vec!["S".to_string(), "M".to_string()],
            vec![],
        ),
        "2" => (
            "002",
            "Prod02",
            "Brand2",
            dec!(50.50),
            vec!["L".to_string(), "XL".to_string()],
            vec!["https://img.com/another_img".to_string()],
        ),
        "3" => ("003", "Prod03", "Brand2", dec!(30.00), vec![], vec![]),
        _ => ("000", "no prod", "no brand", dec!(0.0), vec![], vec![]),
    }
}

// get manually the params from the request
fn get_http_params(str: &str) -> &str {
    let split_line: Vec<&str> = str.split('/').collect();
    // println!("{:?}",split_line);  // ["POST ", "1 HTTP", "1.1"]
    let params: Vec<&str> = split_line[1].split(' ').collect(); // expecting only one params
    params[0]
}

use std::time::SystemTime;

// just playing with tokio
// implementing a bare bone tcp server (mono-thread)
async fn handle_connection(mut stream: TcpStream, cat_cqrs: &mut Catalog_CQRS) {
    // catalog can change in the loop, need to be value and reference mutable
    let bufr = BufReader::new(&mut stream);
    let mut lines = bufr.lines();
    let req_line = lines.next_line().await.unwrap();
    let time_now = SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap()
        .as_secs();
    let resp: String = match req_line {
        Some(str) if str.starts_with("GET") => {
            // getting from the aggregate
            println!("{} - GET request", time_now);
            let inventory = cat_cqrs.query(&ListActiveProducts {
                agg: cat_cqrs.aggregate(),
            });
            // convert to json with serde_json
            let inv_str: Vec<String> = inventory
                .iter()
                .map(|p| format!("Product:{{ {} }}", serde_json::to_string(p).unwrap()))
                .collect();
            let inv_json = inv_str.join(",");
            let res = format!("{}{inv_json}{}", "Catalog:\n[".to_owned(), "\n]".to_owned());
            res
        }
        Some(str) if str.starts_with("POST") => {
            println!("{:?} - POST request", time_now);
            let params = get_http_params(&str);
            let md = mock_prod_data(params);
            cat_cqrs.receive(CreateProduct {
                product_id: md.0.to_string(),
                name: md.1.to_string(),
                brand: md.2.to_string(),
                price: md.3,
                sizes: md.4,
                images: md.5,
            });
            "POST, OK!".to_owned()
        }
        Some(str) if str.starts_with("DELETE") => {
            println!("{:?} - DELETE request", time_now);
            let params = get_http_params(&str);
            cat_cqrs.receive(RemoveProduct {
                prod_id: params.to_string(),
            });
            "DELETE, OK!".to_owned()
        }
        _ => "Error".to_owned(),
    };

    let response = format!(
        "HTTP/1.1 200 OK\r\nContent-Type: application/json\r\nContent-Length: {}\r\n\r\n{}",
        resp.len(),
        resp
    );
    let _ = stream.write(response.as_bytes()).await.unwrap();
    stream.flush().await.unwrap();
}
