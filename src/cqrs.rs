// cq_rs^2
//
// Main module to connect with the CQRS system

use crate::domain::aggregate::{Catalog, Product};
use crate::domain::commands::ProductCatalogCmd;
use crate::domain::design::{Aggregate, EventStore};
use crate::domain::events::ProductCatalogEvent;
use crate::eventstore::CatalogEventStore;
use crate::queries::CatalogQuery;

#[allow(non_camel_case_types)] // just an exception to use the CQRS naming :D
#[derive(Debug, Clone)]
pub struct Catalog_CQRS {
    catalog: Catalog,
    ev_store: CatalogEventStore,
}

impl Catalog_CQRS {
    pub fn new(c: Catalog, ev_store: CatalogEventStore) -> Self {
        Catalog_CQRS {
            catalog: c,
            ev_store,
        }
    }

    pub fn aggregate_id(&self) -> String {
        self.catalog.aggregate_id()
    }

    pub fn aggregate(&self) -> Catalog {
        self.catalog.clone()
    }

    // Fire and forget - receive the msg but don't send any confirmation
    pub fn receive(&mut self, prod_cmd: ProductCatalogCmd) {
        let evs: Vec<ProductCatalogEvent> = self.catalog.decide(prod_cmd);
        for e in evs {
            self.catalog = self.catalog.apply(&e);
            self.ev_store.add(e);
        }
    }

    pub fn query(&self, p_query: &CatalogQuery) -> Vec<Product> {
        match p_query {
            CatalogQuery::ListActiveProducts { agg: _ } => p_query.execute(),
            CatalogQuery::ListDeletedProducts => vec![],
        }
    }
}
