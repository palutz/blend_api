
![Rust Version][rustc-image]
[![crates.io][crate-image]][crate-link]
[![Documentation][docs-image]][docs-link]
[![Dependency Status][deps-image]][deps-link]


Task:

Implement a simple API that allows us to manage a basic product catalog.

Requirements:

A product entity should store the following data:

ID,
name,
brand,
available sizes,
price,
product imagery (links to the images)


The API should allow the user to ADD, REMOVE and LIST products from the catalog


Hints:

You don't need to do any wild things to store data. Keep them in memory (non persistent), store them as JSON or use a tool like duckDB...whatever you prefer. Keep it simple.
You can implement the "API" as plain CLI tool or set up a simple web server (I recommend the axum crate, it's super easy and convenient)

# Overall solution

The solution is a very simplle and quite basic CQRS system with a mock/in-memory events store.

I enjoyed some refactoring to reduce external dependencies to the minimun. I guess with some effort, I could have removed also libs like tokio and serde, but it is ok.

Please see a very high level diagram of the system.

```mermaid
flowchart LR

A[TCP Listener] --> B{Http to CQRS}
B -->|Get| C[Query]
C -->|json| C1[www]
B -->|Post| D[Command CreateProduct]
B -->|Delete| E[Command RemoveProduct]
D --> F[Events]
E --> F[Events]
F --> G[Apply]
G -->|Aggregate| B
```


## How to ...

To run it , from the root folder of the project, just enter:

```
cargo run
```

Open a webbrowser at 
```
http://127.0.0.1:3113
```
It should show a simple page with nothing in our catalog.

### Curl to the rescue

In the root folder there is a text file `curl_test.txt` and inside the commands to run on another terminal to simulate the REST requests.

```
POST => it will create a new product, with a simplified syntax (just pass a number)
DELETE => to remove the specified product (with the product id)
```



[//]: # (badges)

[rustc-image]: https://img.shields.io/badge/rustc-1.60+-blue.svg
[crate-image]: https://img.shields.io/crates/v/{{project-name}}.svg
[crate-link]: https://crates.io/crates/{{project-name}}
[docs-image]: https://docs.rs/{{project-name}}/badge.svg
[docs-link]: https://docs.rs/{{project-name}}
[deps-image]: https://deps.rs/repo/github/palutz/lisp_interpreter_rs/status.svg
[deps-link]: https://deps.rs/repo/github/palutz/lisp_interpreter_rs
